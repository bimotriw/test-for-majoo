import '../../domain/entity/session_entity.dart';

class SessionModel extends SessionEntity {
  SessionModel.fromJson(Map<String, dynamic> json)
      : super(
          id: json['id'] ?? '',
          email: json['email'] ?? '',
          displayName: json['displayName'] ?? '',
          photoUrl: json['photoUrl'] ?? '',
        );
}
