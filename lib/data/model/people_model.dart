import '../../domain/entity/people_entity.dart';

class PeopleModel extends PeopleEntity {
  PeopleModel.fromJson(Map<String, dynamic> json)
      : super(
          id: json['url'] != null
              ? int.parse(json['url'].split('/')[5])
              : json['_id'] ?? 0,
          name: json['name'] ?? '',
          height: json['height'] ?? '',
          mass: json['mass'] ?? '',
          hairColor: json['hair_color'] ?? json['hairColor'] ?? '',
          skinColor: json['skin_color'] ?? json['skinColor'] ?? '',
          eyeColor: json['eye_color'] ?? json['eyeColor'] ?? '',
          birthYear: json['birth_year'] ?? json['birthYear'] ?? '',
          gender: json['gender'] ?? '',
          created: json['created'] ?? '',
          edited: json['edited'] ?? '',
          isFavorite: json['isFavorite'] ?? 0,
        );

  static List<PeopleEntity> parseEntries(List<dynamic> entries) {
    List<PeopleEntity> data = [];
    entries.forEach((value) {
      data.add(PeopleModel.fromJson(value));
    });
    return data;
  }
}
