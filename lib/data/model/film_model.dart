import '../../domain/entity/film_entity.dart';

class FilmModel extends FilmEntity {
  FilmModel.fromJson(Map<String, dynamic> json)
      : super(
          title: json['title'] ?? '',
          openingCrawl: json['opening_crawl'] ?? '',
          director: json['director'] ?? '',
          producer: json['producer'] ?? '',
          releaseDate: json['release_date'] ?? '',
        );

  static List<FilmEntity> parseEntries(List<dynamic> entries) {
    List<FilmEntity> data = [];
    entries.forEach((value) {
      data.add(FilmModel.fromJson(value));
    });
    return data;
  }
}
