import '../../domain/entity/species_entity.dart';

class SpeciesModel extends SpeciesEntity {
  SpeciesModel.fromJson(Map<String, dynamic> json)
      : super(
          name: json['name'] ?? '',
          classification: json['classification'] ?? '',
          designation: json['designation'] ?? '',
          averageHeight: json['average_height'] ?? '',
          skinColors: json['skin_colors'] ?? '',
          hairColors: json['hair_colors'] ?? '',
          eyeColors: json['eye_colors'] ?? '',
          averageLifespan: json['average_lifespan'] ?? '',
          homeworld: json['homeworld'] ?? '',
          language: json['language'] ?? '',
        );

  static List<SpeciesEntity> parseEntries(List<dynamic> entries) {
    List<SpeciesEntity> data = [];
    entries.forEach((value) {
      data.add(SpeciesModel.fromJson(value));
    });
    return data;
  }
}
