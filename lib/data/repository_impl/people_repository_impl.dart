import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';

import '../../core/failure.dart';
import '../../database/database.dart';
import '../../domain/entity/film_entity.dart';
import '../../domain/entity/people_entity.dart';
import '../../domain/entity/species_entity.dart';
import '../../domain/repository/people_repository.dart';
import '../model/film_model.dart';
import '../model/people_model.dart';
import '../model/species_model.dart';

class PeopleRepositoryImpl implements PeopleRepository {
  final Dio client;

  final Logger logger = Logger('PeopleRepositoryImpl');

  PeopleRepositoryImpl({
    @required this.client,
  });

  @override
  Future<Either<Failure, List<dynamic>>> getInitialFetch({int page}) async {
    try {
      Response response = await client.get('people/?page=${page}');
      return Right([
        PeopleModel.parseEntries(response.data['results']),
        response.data['next'] != null
            ? int.parse(response.data['next'].split('?page=')[1])
            : null,
      ]);
    } on DioError catch (e) {
      logger.fine('Error -> ${e}');
      return Left(
        RequestFailure(
          code: e.response.statusCode,
          message: e.response.statusMessage,
        ),
      );
    }
  }

  @override
  Future<List<PeopleEntity>> getLocalData({int page}) async {
    List<PeopleEntity> _data = await NotesDatabase.instance.readAllNotes();
    return _data;
  }

  @override
  Future<List<PeopleEntity>> saveToLocalData(
      {List<PeopleEntity> peoples}) async {
    await peoples.forEach((people) {
      NotesDatabase.instance.create(people);
    });
    List<PeopleEntity> _data = await NotesDatabase.instance.readAllNotes();
    return _data;
  }

  @override
  Future<List<PeopleEntity>> changeFavoriteStatus(
      {PeopleEntity people,
      String order,
      String keyword,
      bool isFavorite}) async {
    await NotesDatabase.instance.favorite(people);
    List<PeopleEntity> _data = await NotesDatabase.instance
        .readAllNotes(keyword: keyword, order: order, isFavorite: isFavorite);
    return _data;
  }

  @override
  Future<List<PeopleEntity>> searchFromLocal(
      {String keyword, String order}) async {
    List<PeopleEntity> _data = await NotesDatabase.instance
        .readAllNotes(keyword: keyword, order: order);
    return _data;
  }

  @override
  Future<List<PeopleEntity>> getLocalFavorites() async {
    List<PeopleEntity> _data =
        await NotesDatabase.instance.readAllNotes(isFavorite: true);
    return _data;
  }

  @override
  Future<void> updatePeopleLocal({PeopleEntity people}) async {
    await NotesDatabase.instance.update(people);
  }

  @override
  Future<void> deletePeopleLocal({int id}) async {
    await NotesDatabase.instance.delete(id);
  }

  @override
  Future<void> addPeopleLocal({PeopleEntity people}) async {
    await NotesDatabase.instance.create(people);
  }

  @override
  Future<List<int>> getTotal() async {
    List<PeopleEntity> _dataFavorite =
        await NotesDatabase.instance.readAllNotes(isFavorite: true);
    List<PeopleEntity> _dataPeople =
        await NotesDatabase.instance.readAllNotes(keyword: '', order: 'ASC');

    return ([_dataPeople.length, _dataFavorite.length]);
  }

  @override
  Future<Either<Failure, List>> getFilmAndSpecies({int id}) async {
    try {
      Response response = await client.get('people/${id}/');

      return Right([
        response.data['films'],
        response.data['species'],
      ]);
    } on DioError catch (e) {
      logger.fine('Error -> ${e}');
      return Left(
        RequestFailure(
          code: e.response.statusCode,
          message: e.response.statusMessage,
        ),
      );
    }
  }

  @override
  Future<FilmEntity> getFilmDetail({int id}) async {
    try {
      Response response = await client.get('films/${id}/');

      return FilmModel.fromJson(response.data);
    } on DioError catch (e) {
      logger.fine('Error -> ${e}');
      return null;
    }
  }

  @override
  Future<SpeciesEntity> getSpeciesDetail({int id}) async {
    try {
      Response response = await client.get('species/${id}/');

      return SpeciesModel.fromJson(response.data);
    } on DioError catch (e) {
      logger.fine('Error -> ${e}');
      return null;
    }
  }
}
