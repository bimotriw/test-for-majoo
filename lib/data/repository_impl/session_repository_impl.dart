import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/failure.dart';
import '../../domain/entity/session_entity.dart';
import '../../domain/repository/session_repository.dart';
import '../model/session_model.dart';

class SessionRepositoryImpl extends SessionRepository {
  static const String userKey = 'user';

  final Logger logger = Logger('SessionRepositoryImpl');

  @override
  Future<Either<Failure, SessionEntity>> load() async {
    SharedPreferences sp = await SharedPreferences.getInstance();

    if (!sp.containsKey(userKey)) {
      return Left(CacheFailure());
    }

    final Map<String, dynamic> userMap = json.decode(
      sp.getString(userKey),
    );

    return Right(SessionModel.fromJson(userMap));
  }

  @override
  Future<Either<Failure, bool>> delete() async {
    SharedPreferences sp = await SharedPreferences.getInstance();

    await sp.remove(userKey);

    return Right(true);
  }

  @override
  Future<Either<Failure, bool>> save({SessionEntity session}) async {
    SharedPreferences sp = await SharedPreferences.getInstance();

    await sp.setString(
      userKey,
      json.encode(
        {
          'id': session.id,
          'email': session.email,
          'displayName': session.displayName,
          'photoUrl': session.photoUrl,
        },
      ),
    );

    return Right(true);
  }
}
