import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:logging/logging.dart';
import 'config/constant_config.dart';

import 'presentation/core/app.dart';
import 'presentation/core/main_app.dart';

Future<Null> main() async {
  // Activate logger in root
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((record) {
    print(
      '${record.level.name}: ${record.loggerName}: ${record.time}: ${record.message}',
    );
  });

  GetIt.instance.registerSingleton(
    App(
      baseUrl: Config.baseUrl,
      googleSignIn: GoogleSignIn(),
    ),
  );

  // ensure initialized
  WidgetsFlutterBinding.ensureInitialized();

  // wait initialized
  await App.main.init();

  runApp(MainApp());
}
