import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:meta/meta.dart';

import '../../core/failure.dart';
import '../../domain/entity/session_entity.dart';
import '../../domain/repository/session_repository.dart';

abstract class SessionState extends Equatable {
  @override
  List<Object> get props => [];
}

class SessionInitialState extends SessionState {}

class SessionLoadingState extends SessionState {}

class SessionLocalLoadedState extends SessionState {
  final SessionEntity session;

  SessionLocalLoadedState({this.session});

  @override
  List<Object> get props => [session];
}

class SessionLocalEmptyState extends SessionState {}

class SessionBloc extends Cubit<SessionState> {
  final SessionRepository repository;

  final Logger logger = Logger('SessionBloc');

  bool isInit = true;

  SessionBloc({@required this.repository}) : super(SessionInitialState());

  Future<void> loadLocal() async {
    Either<Failure, SessionEntity> result = await repository.load();

    SessionState state = result.fold(
      (failure) {
        logger.fine('Status Session -> Local session data not found');
        return SessionLocalEmptyState();
      },
      (s) {
        logger.fine('Status Session -> Local session data found');
        logger.fine('Session email -> ${s.email}');
        logger.fine('Session displayName -> ${s.displayName}');
        logger.fine('Session photoUrl -> ${s.photoUrl}');
        return SessionLocalLoadedState(session: s);
      },
    );

    emit(state);
  }

  Future<void> createSession(SessionEntity session) async {
    emit(SessionLoadingState());

    await repository
        .save(
      session: session,
    )
        .then(
      (value) {
        logger.fine('Created session has been saved in shared preferences');

        emit(SessionLocalLoadedState(session: session));
      },
    );
  }

  Future<void> removeSession() async {
    emit(SessionLoadingState());

    await repository.delete().then(
      (value) {
        logger.fine('Session has been removed from shared preferences');

        emit(SessionInitialState());
      },
    );
  }
}
