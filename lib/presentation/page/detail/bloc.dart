import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import '../../../domain/entity/film_entity.dart';
import '../../../domain/entity/species_entity.dart';
import 'package:meta/meta.dart';

import '../../../core/failure.dart';
import '../../../domain/entity/people_entity.dart';
import '../../../domain/repository/people_repository.dart';

// Start of state
abstract class DetailState extends Equatable {
  final List<FilmEntity> films;
  final List<SpeciesEntity> species;

  DetailState({this.films, this.species});

  @override
  List<Object> get props => [films, species];
}

class DetailInitialState extends DetailState {}

class DetailLoadingState extends DetailState {
  final DateTime time;

  DetailLoadingState({List<FilmEntity> films, List<SpeciesEntity> species})
      : time = DateTime.now(),
        super(films: films, species: species);

  @override
  List<Object> get props => [time];
}

class DetailSuccessState extends DetailState {}

class DetailFailedState extends DetailState {
  final int code;
  final String message;

  DetailFailedState(this.code, this.message,
      {List<FilmEntity> films, List<SpeciesEntity> species})
      : super(films: films, species: species);
}
//End state

class DetailBloc extends Cubit<DetailState> {
  final PeopleRepository repository;

  final Logger logger = Logger('DetailBloc');

  GlobalKey<FormState> keyForm = GlobalKey<FormState>();
  TextEditingController fullNameController = TextEditingController();
  TextEditingController heightController = TextEditingController();
  TextEditingController massController = TextEditingController();
  TextEditingController hairColorController = TextEditingController();
  TextEditingController skinColorController = TextEditingController();
  TextEditingController eyeColorController = TextEditingController();
  TextEditingController birthYearController = TextEditingController();
  TextEditingController genderController = TextEditingController();

  DetailBloc({@required this.repository}) : super(DetailInitialState());

  Future<void> getDetail(int id) async {
    emit(DetailLoadingState());

    Either<Failure, List<dynamic>> result = await repository.getFilmAndSpecies(
      id: id,
    );

    result.fold(
      (failure) {
        RequestFailure f = failure;
        return emit(DetailFailedState(f.code, f.message));
      },
      (s) {
        logger.fine('Success Data -> ${s}');

        List<FilmEntity> _dataFilms = [];
        List<SpeciesEntity> _dataSpecies = [];

        return Future.forEach(
          s[0],
          (element) async {
            FilmEntity resultGetFilm = await repository.getFilmDetail(
                id: int.parse(element.split('/')[5]));

            if (resultGetFilm != null) {
              _dataFilms.add(resultGetFilm);
            }
          },
        ).whenComplete(
          () => Future.forEach(
            s[1],
            (element) async {
              SpeciesEntity resultGetSpecies = await repository
                  .getSpeciesDetail(id: int.parse(element.split('/')[5]));

              if (resultGetSpecies != null) {
                _dataSpecies.add(resultGetSpecies);
              }
            },
          ).whenComplete(() => emit(
              DetailLoadingState(films: _dataFilms, species: _dataSpecies))),
        );
      },
    );
  }

  Future<void> update(PeopleEntity people) async {
    emit(DetailLoadingState(films: state.films, species: state.species));

    await repository.updatePeopleLocal(people: people);

    emit(DetailSuccessState());
  }

  Future<void> addNew(PeopleEntity people) async {
    emit(DetailLoadingState(films: state.films, species: state.species));

    await repository.addPeopleLocal(people: people);

    emit(DetailSuccessState());
  }
}
