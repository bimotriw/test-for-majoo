import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/repository_impl/people_repository_impl.dart';
import '../../../domain/entity/film_entity.dart';
import '../../../domain/entity/people_entity.dart';
import '../../../domain/entity/species_entity.dart';
import '../../../extension/context_extension.dart';
import '../../config/style_config.dart';
import '../../core/app.dart';
import '../main_menu/page.dart';
import 'bloc.dart';

class DetailPage extends StatefulWidget {
  final PeopleEntity people;
  final bool isEdit;

  DetailPage({Key key, this.people, this.isEdit = false}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  DetailBloc _detailBloc;

  @override
  void initState() {
    _detailBloc = DetailBloc(
      repository: PeopleRepositoryImpl(
        client: App.main.client,
      ),
    );

    if (widget.people != null && !widget.isEdit) {
      _detailBloc..getDetail(widget.people.id);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _detailBloc,
      child: BlocListener<DetailBloc, DetailState>(
        listener: (context, state) {
          if (state is DetailSuccessState) {
            Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (context) => MainMenuPage(),
              ),
              (route) => false,
            );
          }
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text(widget.people == null
                ? 'New'
                : !widget.isEdit
                    ? 'Detail'
                    : 'Edit'),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(16),
              child: widget.people == null
                  ? Form(
                      key: _detailBloc.keyForm,
                      child: Column(
                        children: [
                          _FieldForm(
                            label: 'Full Name',
                            controller: _detailBloc.fullNameController,
                          ),
                          _FieldForm(
                            label: 'Height',
                            controller: _detailBloc.heightController,
                          ),
                          _FieldForm(
                            label: 'Mass',
                            controller: _detailBloc.massController,
                          ),
                          _FieldForm(
                            label: 'Hair Color',
                            controller: _detailBloc.hairColorController,
                          ),
                          _FieldForm(
                            label: 'Skin Color',
                            controller: _detailBloc.skinColorController,
                          ),
                          _FieldForm(
                            label: 'Eye Color',
                            controller: _detailBloc.eyeColorController,
                          ),
                          _FieldForm(
                            label: 'Birth Year',
                            controller: _detailBloc.birthYearController,
                          ),
                          _FieldForm(
                            label: 'Gender',
                            controller: _detailBloc.genderController,
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: ElevatedButton(
                              onPressed: () {
                                if (!_detailBloc.keyForm.currentState
                                    .validate()) {
                                  return;
                                }
                                _detailBloc.addNew(PeopleEntity(
                                  name: _detailBloc.fullNameController.text
                                      .trim(),
                                  height:
                                      _detailBloc.heightController.text.trim(),
                                  mass: _detailBloc.massController.text.trim(),
                                  hairColor: _detailBloc
                                      .hairColorController.text
                                      .trim(),
                                  skinColor: _detailBloc
                                      .skinColorController.text
                                      .trim(),
                                  eyeColor: _detailBloc.fullNameController.text
                                      .trim(),
                                  birthYear: _detailBloc
                                      .birthYearController.text
                                      .trim(),
                                  gender:
                                      _detailBloc.genderController.text.trim(),
                                  created: DateTime.now().toString(),
                                  edited: DateTime.now().toString(),
                                ));
                              },
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Icon(
                                    Icons.save,
                                    color: Colors.white,
                                  ),
                                  SizedBox(width: 8),
                                  Text('Add New'),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : !widget.isEdit
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            _Field(
                                label: 'Full Name', value: widget.people.name),
                            _Field(label: 'Height', value: widget.people.name),
                            _Field(label: 'Mass', value: widget.people.mass),
                            _Field(
                                label: 'Hair Color',
                                value: widget.people.hairColor),
                            _Field(
                                label: 'Skin Color',
                                value: widget.people.skinColor),
                            _Field(
                                label: 'Eye Color',
                                value: widget.people.eyeColor),
                            _Field(
                                label: 'Birth Year',
                                value: widget.people.birthYear),
                            _Field(
                                label: 'Gender', value: widget.people.gender),
                            BlocBuilder<DetailBloc, DetailState>(
                              builder: (context, state) {
                                if (state.species == null) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }
                                return Column(
                                  children: [
                                    state.films != null &&
                                            state.films.length > 0
                                        ? Text(
                                            'Films',
                                            style: context.textTheme.headline6,
                                          )
                                        : SizedBox(),
                                    Column(
                                      children: List.generate(
                                        state.films == null
                                            ? 0
                                            : state.films.length,
                                        (index) =>
                                            _FilmTile(film: state.films[index]),
                                      ),
                                    ),
                                    SizedBox(height: 16),
                                    state.species != null &&
                                            state.species.length > 0
                                        ? Text(
                                            'Species',
                                            style: context.textTheme.headline6,
                                          )
                                        : SizedBox(),
                                    Column(
                                      children: List.generate(
                                        state.species == null
                                            ? 0
                                            : state.species.length,
                                        (index) => _SpeciesTile(
                                            species: state.species[index]),
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ),
                          ],
                        )
                      : Form(
                          key: _detailBloc.keyForm,
                          child: Column(
                            children: [
                              _FieldForm(
                                label: 'Full Name',
                                value: widget.people.name,
                                controller: _detailBloc.fullNameController,
                              ),
                              _FieldForm(
                                label: 'Height',
                                value: widget.people.height,
                                controller: _detailBloc.heightController,
                              ),
                              _FieldForm(
                                label: 'Mass',
                                value: widget.people.mass,
                                controller: _detailBloc.massController,
                              ),
                              _FieldForm(
                                label: 'Hair Color',
                                value: widget.people.hairColor,
                                controller: _detailBloc.hairColorController,
                              ),
                              _FieldForm(
                                label: 'Skin Color',
                                value: widget.people.skinColor,
                                controller: _detailBloc.skinColorController,
                              ),
                              _FieldForm(
                                label: 'Eye Color',
                                value: widget.people.eyeColor,
                                controller: _detailBloc.eyeColorController,
                              ),
                              _FieldForm(
                                label: 'Birth Year',
                                value: widget.people.birthYear,
                                controller: _detailBloc.birthYearController,
                              ),
                              _FieldForm(
                                label: 'Gender',
                                value: widget.people.gender,
                                controller: _detailBloc.genderController,
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: ElevatedButton(
                                  onPressed: () {
                                    if (!_detailBloc.keyForm.currentState
                                        .validate()) {
                                      return;
                                    }
                                    showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                        title: Text('Alert!'),
                                        content: Text(
                                            'Are you sure you want to update this data?'),
                                        actions: [
                                          IconButton(
                                            onPressed: () {
                                              _detailBloc.update(
                                                  widget.people.copyWith(
                                                name: _detailBloc
                                                    .fullNameController.text
                                                    .trim(),
                                                height: _detailBloc
                                                    .heightController.text
                                                    .trim(),
                                                mass: _detailBloc
                                                    .massController.text
                                                    .trim(),
                                                hairColor: _detailBloc
                                                    .hairColorController.text
                                                    .trim(),
                                                skinColor: _detailBloc
                                                    .skinColorController.text
                                                    .trim(),
                                                eyeColor: _detailBloc
                                                    .fullNameController.text
                                                    .trim(),
                                                birthYear: _detailBloc
                                                    .birthYearController.text
                                                    .trim(),
                                                gender: _detailBloc
                                                    .genderController.text
                                                    .trim(),
                                              ));
                                            },
                                            icon: Icon(Icons.check),
                                          ),
                                          IconButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            icon: Icon(Icons.close),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Icon(
                                        Icons.save,
                                        color: Colors.white,
                                      ),
                                      SizedBox(width: 8),
                                      Text('Update'),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
            ),
          ),
        ),
      ),
    );
  }
}

class _Field extends StatelessWidget {
  final String label;
  final String value;

  _Field({Key key, this.label, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: context.textTheme.headline6,
        ),
        Text(
          value,
          style: context.textTheme.bodyText1.copyWith(
            color: AppColor.PRIMARY,
          ),
        ),
        SizedBox(height: 16),
      ],
    );
  }
}

class _FieldForm extends StatefulWidget {
  final TextEditingController controller;
  final String label;
  final String value;

  _FieldForm({Key key, this.controller, this.label, this.value = ''})
      : super(key: key);

  @override
  __FieldFormState createState() => __FieldFormState();
}

class __FieldFormState extends State<_FieldForm> {
  @override
  void initState() {
    widget.controller.text = widget.value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormField(
          controller: widget.controller,
          decoration: InputDecoration(
            labelText: widget.label,
          ),
          validator: (val) {
            if (val.trim().isEmpty) {
              return '*Required';
            }
            return null;
          },
        ),
        SizedBox(height: 16),
      ],
    );
  }
}

class _FilmTile extends StatelessWidget {
  final FilmEntity film;

  _FilmTile({Key key, this.film}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 7,
            offset: Offset(0, 8), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            film.title,
            style: context.textTheme.headline6,
          ),
          Text('Director : ${film.director}'),
          Text('Producer : ${film.producer}'),
          Text('Release Date : ${film.releaseDate}'),
        ],
      ),
    );
  }
}

class _SpeciesTile extends StatelessWidget {
  final SpeciesEntity species;

  _SpeciesTile({Key key, this.species}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 7,
            offset: Offset(0, 8), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            species.name,
            style: context.textTheme.headline6,
          ),
          Text('Classification : ${species.classification}'),
          Text('Designation : ${species.designation}'),
          Text('Average Height : ${species.averageHeight}'),
          Text('Skin Colors : ${species.skinColors}'),
          Text('Hair Colors : ${species.hairColors}'),
          Text('Eye Colors : ${species.eyeColors}'),
          Text('Homeworld : ${species.homeworld}'),
          Text('Language : ${species.language}'),
        ],
      ),
    );
  }
}
