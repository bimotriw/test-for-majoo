import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/repository_impl/people_repository_impl.dart';
import '../../../extension/context_extension.dart';
import '../../config/style_config.dart';
import '../../core/app.dart';
import '../detail/page.dart';
import 'bloc.dart';

class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  FavoriteBloc _favoriteBloc;

  @override
  void initState() {
    _favoriteBloc = FavoriteBloc(
      repository: PeopleRepositoryImpl(
        client: App.main.client,
      ),
    )..getFavorites();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _favoriteBloc,
      child: BlocBuilder<FavoriteBloc, FavoriteState>(
        builder: (context, state) {
          if (state is FavoriteLoadedState) {
            return ListView(
              children: List.generate(
                state.peoples.length,
                (index) => Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: 8,
                  ),
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 7,
                        offset: Offset(0, 8), // changes position of shadow
                      ),
                    ],
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                state.peoples[index].name,
                                style: context.textTheme.headline6,
                              ),
                              Text(
                                'Gender : ${state.peoples[index].gender}',
                                style: context.textTheme.bodyText1,
                              ),
                              Text(
                                'Birth Year : ${state.peoples[index].birthYear}',
                                style: context.textTheme.bodyText1,
                              ),
                            ],
                          ),
                          IconButton(
                            onPressed: () {
                              _favoriteBloc.changeToFavorite(state.peoples[index]);
                            },
                            icon: Icon(
                              state.peoples[index].isFavorite == 0
                                  ? Icons.star_border
                                  : Icons.star,
                              size: 32,
                              color: AppColor.PRIMARY,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 8),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => DetailPage(
                                people: state.peoples[index],
                              ),
                            ),
                          );
                        },
                        child: Text('Detail'),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
          return SizedBox();
        },
      ),
    );
  }
}
