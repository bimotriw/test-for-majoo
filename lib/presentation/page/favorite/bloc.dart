import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:meta/meta.dart';

import '../../../domain/entity/people_entity.dart';
import '../../../domain/repository/people_repository.dart';

// Start of state
abstract class FavoriteState extends Equatable {
  @override
  List<Object> get props => [];
}

class FavoriteInitialState extends FavoriteState {}

class FavoriteLoadingState extends FavoriteState {}

class FavoriteLoadedState extends FavoriteState {
  final DateTime time;

  final List<PeopleEntity> peoples;

  FavoriteLoadedState(this.peoples)
      : time = DateTime.now(),
        super();

  @override
  List<Object> get props => [peoples, time];
}
//End state

class FavoriteBloc extends Cubit<FavoriteState> {
  final PeopleRepository repository;

  final Logger logger = Logger('FavoriteBloc');

  FavoriteBloc({@required this.repository}) : super(FavoriteInitialState());

  Future<void> getFavorites() async {
    emit(FavoriteLoadingState());

    List<PeopleEntity> result = await repository.getLocalFavorites();

    emit(FavoriteLoadedState(result));
  }

  Future<void> changeToFavorite(PeopleEntity people) async {
    List<PeopleEntity> result = await repository.changeFavoriteStatus(
        keyword: '', order: 'ASC', people: people, isFavorite: true);

    emit(FavoriteLoadedState(result));
  }
}
