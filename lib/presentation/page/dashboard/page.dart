import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/repository_impl/people_repository_impl.dart';
import '../../../domain/entity/people_entity.dart';
import '../../../extension/context_extension.dart';
import '../../config/style_config.dart';
import '../../core/app.dart';
import '../detail/page.dart';
import 'bloc.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  DashboardBloc _dashboardBloc;

  @override
  void initState() {
    _dashboardBloc = DashboardBloc(
      repository: PeopleRepositoryImpl(
        client: App.main.client,
      ),
    )..getListData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => _dashboardBloc),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<DashboardBloc, DashboardState>(
            listener: (context, state) {
              if (state is DashboardLoadedState) {
                _dashboardBloc.saveToLocal();
              } else if (state is DashboardFailedState) {
                Flushbar(
                  backgroundColor: AppColor.ERROR.withOpacity(0.8),
                  message: 'Code: ${state.code} => ${state.message}',
                  duration: Duration(seconds: 3),
                ).show(context);
              }
            },
          ),
        ],
        child: BlocBuilder<DashboardBloc, DashboardState>(
          builder: (context, state) {
            if (state is DashboardLoadedFromLocalState) {
              return Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(16),
                    child: Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _dashboardBloc.searchController,
                            onChanged: (val) {
                              _dashboardBloc.search(val.trim());
                            },
                            decoration: InputDecoration(
                              hintText: 'Search',
                              prefixIcon: Icon(
                                Icons.search,
                                color: AppColor.BASIC,
                              ),
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            _dashboardBloc.changeView(true);
                          },
                          icon: Icon(
                            Icons.list,
                            color: _dashboardBloc.isListView
                                ? AppColor.BASIC
                                : AppColor.PRIMARY,
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            _dashboardBloc.changeView(false);
                          },
                          icon: Icon(
                            Icons.grid_on_outlined,
                            color: !_dashboardBloc.isListView
                                ? AppColor.BASIC
                                : AppColor.PRIMARY,
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            _dashboardBloc.changeOrder(
                                _dashboardBloc.order == 'ASC' ? 'DESC' : 'ASC');
                          },
                          icon: Text(_dashboardBloc.order),
                        ),
                        IconButton(
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => DetailPage(),
                              ),
                            );
                          },
                          icon: Icon(
                            Icons.add,
                            color: AppColor.BASIC,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: _dashboardBloc.isListView
                        ? _ListViewContent(peoples: state.peoples)
                        : _GridViewContent(peoples: state.peoples),
                  ),
                ],
              );
            } else if (state is DashboardFailedState) {
              return Center(
                child: GestureDetector(
                  onTap: () {
                    _dashboardBloc.getListData();
                  },
                  child: Text('Try Again.'),
                ),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}

class _ListViewContent extends StatelessWidget {
  final List<PeopleEntity> peoples;

  _ListViewContent({Key key, this.peoples}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: List.generate(
        peoples.length,
        (index) => Container(
          margin: EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 8,
          ),
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 7,
                offset: Offset(0, 8), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        peoples[index].name,
                        style: context.textTheme.headline6,
                      ),
                      Text(
                        'Gender : ${peoples[index].gender}',
                        style: context.textTheme.bodyText1,
                      ),
                      Text(
                        'Birth Year : ${peoples[index].birthYear}',
                        style: context.textTheme.bodyText1,
                      ),
                    ],
                  ),
                  IconButton(
                    onPressed: () {
                      context
                          .read<DashboardBloc>()
                          .changeToFavorite(peoples[index]);
                    },
                    icon: Icon(
                      peoples[index].isFavorite == 0
                          ? Icons.star_border
                          : Icons.star,
                      size: 32,
                      color: AppColor.PRIMARY,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => DetailPage(
                              people: peoples[index],
                            ),
                          ),
                        );
                      },
                      child: Text('Detail'),
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => DetailPage(
                            people: peoples[index],
                            isEdit: true,
                          ),
                        ),
                      );
                    },
                    icon: Icon(
                      Icons.edit,
                      size: 32,
                      color: AppColor.BASIC,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (_) => AlertDialog(
                          title: Text('Alert!'),
                          content: Text(
                              'Are you sure you want to delete this data?'),
                          actions: [
                            IconButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                                context
                                    .read<DashboardBloc>()
                                    .deleteLocalPeople(peoples[index].id);
                              },
                              icon: Icon(Icons.check),
                            ),
                            IconButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              icon: Icon(Icons.close),
                            ),
                          ],
                        ),
                      );
                    },
                    icon: Icon(
                      Icons.delete_outline,
                      size: 32,
                      color: AppColor.ERROR,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _GridViewContent extends StatelessWidget {
  final List<PeopleEntity> peoples;

  _GridViewContent({Key key, this.peoples}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: EdgeInsets.all(16),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 16,
        mainAxisSpacing: 16,
        childAspectRatio: 0.8,
      ),
      children: List.generate(
        peoples.length,
        (index) => Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 7,
                offset: Offset(0, 8), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Text(
                    peoples[index].name,
                    style: context.textTheme.headline6,
                  ),
                  Text(
                    'Gender : ${peoples[index].gender}',
                    style: context.textTheme.bodyText1,
                  ),
                  Text(
                    'Birth Year : ${peoples[index].birthYear}',
                    style: context.textTheme.bodyText1,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => DetailPage(
                            people: peoples[index],
                          ),
                        ),
                      );
                    },
                    icon: Icon(
                      Icons.person_outline,
                      color: AppColor.BASIC,
                      size: 32,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      context
                          .read<DashboardBloc>()
                          .changeToFavorite(peoples[index]);
                    },
                    icon: Icon(
                      peoples[index].isFavorite == 0
                          ? Icons.star_border
                          : Icons.star,
                      size: 32,
                      color: AppColor.ERROR,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => DetailPage(
                            people: peoples[index],
                            isEdit: true,
                          ),
                        ),
                      );
                    },
                    icon: Icon(
                      Icons.edit,
                      size: 32,
                      color: AppColor.BASIC,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (_) => AlertDialog(
                          title: Text('Alert!'),
                          content: Text(
                              'Are you sure you want to delete this data?'),
                          actions: [
                            IconButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                                context
                                    .read<DashboardBloc>()
                                    .deleteLocalPeople(peoples[index].id);
                              },
                              icon: Icon(Icons.check),
                            ),
                            IconButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              icon: Icon(Icons.close),
                            ),
                          ],
                        ),
                      );
                    },
                    icon: Icon(
                      Icons.delete_outline,
                      size: 32,
                      color: AppColor.ERROR,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
