import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:meta/meta.dart';

import '../../../core/failure.dart';
import '../../../domain/entity/people_entity.dart';
import '../../../domain/repository/people_repository.dart';

// Start of state
abstract class DashboardState extends Equatable {
  final List<PeopleEntity> peoples;

  DashboardState({this.peoples});

  @override
  List<Object> get props => [peoples];
}

class DashboardInitialState extends DashboardState {}

class DashboardLoadingState extends DashboardState {
  DashboardLoadingState({List<PeopleEntity> peoples}) : super(peoples: peoples);
}

class DashboardLoadedState extends DashboardState {
  DashboardLoadedState(List<PeopleEntity> peoples) : super(peoples: peoples);
}

class DashboardLoadedFromLocalState extends DashboardState {
  final DateTime time;

  DashboardLoadedFromLocalState(List<PeopleEntity> peoples)
      : time = DateTime.now(),
        super(peoples: peoples);

  @override
  List<Object> get props => [time];
}

class DashboardFailedState extends DashboardState {
  final int code;
  final String message;

  DashboardFailedState(this.code, this.message, {List<PeopleEntity> peoples})
      : super(peoples: peoples);
}
//End state

class DashboardBloc extends Cubit<DashboardState> {
  final PeopleRepository repository;

  final Logger logger = Logger('DashboardBloc');

  final ScrollController scrollController = ScrollController();

  final TextEditingController searchController = TextEditingController();

  int page = 1;

  DashboardState _currentStateWhileFetchingRecursive;

  bool isListView = true;

  String order = 'ASC';

  DashboardBloc({@required this.repository}) : super(DashboardInitialState());

  Future<DashboardState> fetchListData({int page}) async {
    emit(DashboardLoadingState(peoples: state.peoples));

    Either<Failure, List<dynamic>> result = await repository.getInitialFetch(
      page: page,
    );

    DashboardState stateResult = result.fold(
      (failure) {
        RequestFailure f = failure;
        return DashboardFailedState(f.code, f.message);
      },
      (s) {
        logger.fine('Success Data -> ${s}');

        List<PeopleEntity> _data = state.peoples ?? [];
        _data.addAll(s[0] as List<PeopleEntity>);

        this.page = s[1];

        return DashboardLoadingState(peoples: _data);
      },
    );

    if (stateResult is DashboardLoadingState) {
      emit(stateResult);
    }
    return stateResult;
  }

  Future<void> recursiveForInitialFetch() async {
    Future.doWhile(() async {
      _currentStateWhileFetchingRecursive = await fetchListData(page: page);
      return (page != null &&
          !(_currentStateWhileFetchingRecursive is DashboardFailedState));
    }).whenComplete(() {
      if (_currentStateWhileFetchingRecursive is DashboardFailedState) {
        emit(_currentStateWhileFetchingRecursive);
      } else {
        emit(DashboardLoadedState(state.peoples));
      }
    });
  }

  Future<void> getListData() async {
    emit(DashboardLoadingState());

    List<PeopleEntity> result = await repository.getLocalData(
      page: page,
    );

    if (result.length != 0) {
      return emit(DashboardLoadedFromLocalState(result));
    }
    return recursiveForInitialFetch();
  }

  Future<void> saveToLocal() async {
    List<PeopleEntity> result = await repository.saveToLocalData(
      peoples: state.peoples,
    );

    emit(DashboardLoadedFromLocalState(result));
  }

  Future<void> changeToFavorite(PeopleEntity people) async {
    List<PeopleEntity> result = await repository.changeFavoriteStatus(
      people: people,
      order: order,
      keyword: searchController.text.trim(),
      isFavorite: false,
    );

    emit(DashboardLoadedFromLocalState(result));
  }

  Future<void> search(String keyword) async {
    List<PeopleEntity> result = await repository.searchFromLocal(
      keyword: keyword,
      order: order,
    );

    emit(DashboardLoadedFromLocalState(result));
  }

  Future<void> changeView(bool isListView) async {
    this.isListView = isListView;

    emit(DashboardLoadedFromLocalState(state.peoples));
  }

  Future<void> changeOrder(String order) async {
    this.order = order;

    List<PeopleEntity> result = await repository.searchFromLocal(
      keyword: searchController.text.trim(),
      order: order,
    );

    emit(DashboardLoadedFromLocalState(result));
  }

  Future<void> deleteLocalPeople(int id) async {
    await repository.deletePeopleLocal(id: id);

    List<PeopleEntity> _data = state.peoples;
    _data.removeWhere((people) => people.id == id);

    emit(DashboardLoadedFromLocalState(_data));
  }
}
