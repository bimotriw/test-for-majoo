import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../bloc/session_bloc.dart';

import '../../config/style_config.dart';
import '../../widget/block_loader_widget.dart';
import '../../widget/logo_widget.dart';
import '../main_menu/page.dart';
import 'bloc.dart';

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  AuthBloc _authBloc;

  @override
  void initState() {
    _authBloc = AuthBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _authBloc,
      child: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) async {
          if (state is AuthSuccessState) {
            await context.read<SessionBloc>().createSession(state.user);
            Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (context) => MainMenuPage(),
              ),
              (route) => false,
            );
          } else if (state is AuthLoadingState) {
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) => BlockLoaderWidget(),
            );
          } else if (state is AuthFailedState) {
            Navigator.pop(context);
          }
        },
        child: Scaffold(
          body: DoubleBackToCloseApp(
            snackBar: SnackBar(
              backgroundColor: AppColor.BASIC,
              content: Text(
                'Tap back again to close app',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: LogoWidget(),
                ),
                SizedBox(height: 40),
                ElevatedButton(
                  onPressed: () {
                    _authBloc.signIn();
                  },
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image(
                        image: AssetImage(
                          'assets/images/google-logo.png',
                        ),
                        height: 32,
                      ),
                      SizedBox(width: 16),
                      Text('Sign In With Google'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
