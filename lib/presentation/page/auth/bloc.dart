import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:logging/logging.dart';

import '../../../domain/entity/session_entity.dart';
import '../../core/app.dart';

// Start of state
abstract class AuthState extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthInitialState extends AuthState {}

class AuthLoadingState extends AuthState {}

class AuthSuccessState extends AuthState {
  final SessionEntity user;

  AuthSuccessState(this.user);

  @override
  List<Object> get props => [user];
}

class AuthFailedState extends AuthState {}
//End state

class AuthBloc extends Cubit<AuthState> {
  final Logger logger = Logger('AuthBloc');

  AuthBloc() : super(AuthInitialState());

  Future<void> signIn() async {
    emit(AuthLoadingState());
    GoogleSignInAccount user = await App.main.googleSignIn.signIn();

    if (user != null) {
      final googleAuth = await user.authentication;

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      await FirebaseAuth.instance.signInWithCredential(credential);

      logger.fine('Auth Google Success');
      return emit(
        AuthSuccessState(
          SessionEntity(
            id: user.id,
            email: user.email,
            displayName: user.displayName,
            photoUrl: user.photoUrl,
          ),
        ),
      );
    }
    logger.fine('Auth Google Failed or Canceled');
    emit(AuthFailedState());
  }
}
