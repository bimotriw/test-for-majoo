import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/session_bloc.dart';
import '../../widget/logo_widget.dart';
import '../auth/page.dart';
import '../main_menu/page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    context.read<SessionBloc>()..loadLocal();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SessionBloc, SessionState>(
      listener: (BuildContext context, SessionState state) async {
        await Future.delayed(Duration(seconds: 2));
        if (state is SessionLocalLoadedState) {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (context) => MainMenuPage(),
            ),
          );
        } else {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (context) => AuthPage(),
            ),
          );
        }
      },
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 64),
          child: LogoWidget(),
        ),
      ),
    );
  }
}
