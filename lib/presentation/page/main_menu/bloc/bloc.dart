import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';

// Start of state
abstract class MainMenuState extends Equatable {
  @override
  List<Object> get props => [];
}

class MainMenuInitialState extends MainMenuState {}

class MainMenuLoadingState extends MainMenuState {
  final int time;

  MainMenuLoadingState()
      : time = DateTime.now().millisecondsSinceEpoch,
        super();

  @override
  List<Object> get props => [time];
}
//End state

class MainMenuBloc extends Cubit<MainMenuState> {
  int selectedIndex = 0;

  final Logger logger = Logger('MainMenuBloc');

  MainMenuBloc() : super(MainMenuInitialState());

  Future<void> changeBottomNav(int index) async {
    selectedIndex = index;
    emit(MainMenuLoadingState());
  }
}
