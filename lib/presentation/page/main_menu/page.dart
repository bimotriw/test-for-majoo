import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../config/style_config.dart';
import '../dashboard/page.dart';
import '../favorite/page.dart';
import '../profile/page.dart';
import 'bloc/bloc.dart';

class MainMenuPage extends StatefulWidget {
  @override
  _MainMenuPageState createState() => _MainMenuPageState();
}

class _MainMenuPageState extends State<MainMenuPage> {
  MainMenuBloc _mainMenuBloc;

  @override
  void initState() {
    _mainMenuBloc = MainMenuBloc();
    super.initState();
  }

  List<dynamic> _optionSelection = [
    {
      'name': 'Beranda',
      'widget': DashboardPage(),
    },
    {
      'name': 'Favorit',
      'widget': FavoritePage(),
    },
    {
      'name': 'Profil',
      'widget': ProfilePage(),
    },
  ];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _mainMenuBloc,
      child: BlocBuilder<MainMenuBloc, MainMenuState>(
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title:
                  Text(_optionSelection[_mainMenuBloc.selectedIndex]['name']),
            ),
            body: _optionSelection[_mainMenuBloc.selectedIndex]['widget'],
            bottomNavigationBar: BottomNavigationBar(
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.supervised_user_circle_outlined),
                  label: 'Beranda',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.star),
                  label: 'Favorit',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person_outline),
                  label: 'Profil',
                ),
              ],
              currentIndex: _mainMenuBloc.selectedIndex,
              selectedItemColor: AppColor.PRIMARY,
              onTap: (index) {
                _mainMenuBloc.changeBottomNav(index);
              },
            ),
          );
        },
      ),
    );
  }
}
