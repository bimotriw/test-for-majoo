import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/repository_impl/people_repository_impl.dart';
import '../../../extension/context_extension.dart';
import '../../bloc/session_bloc.dart';
import '../../config/style_config.dart';
import '../../core/app.dart';
import '../../widget/cache_avatar_image_widget.dart';
import '../auth/page.dart';
import 'bloc.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  ProfileBloc _profileBloc;

  @override
  void initState() {
    _profileBloc = ProfileBloc(
      repository: PeopleRepositoryImpl(
        client: App.main.client,
      ),
    )..getTotal();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _profileBloc,
      child: BlocListener<ProfileBloc, ProfileState>(
        listener: (context, state) async {
          if (state is ProfileSignOutState) {
            await context.read<SessionBloc>().removeSession();
            Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (context) => AuthPage(),
              ),
              (route) => false,
            );
          }
        },
        child: BlocBuilder<SessionBloc, SessionState>(
          builder: (context, state) {
            if (state is SessionLocalLoadedState) {
              return SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(32),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CacheAvatarImageWidget(
                        photoUrl: state.session.photoUrl,
                        radius: 60,
                      ),
                      SizedBox(height: 30),
                      Text(
                        state.session.displayName,
                        style: context.textTheme.headline6,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 8),
                      Text(
                        state.session.email,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 30,
                        width: double.infinity,
                      ),
                      BlocBuilder<ProfileBloc, ProfileState>(
                        builder: (context, state) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              _CountWidget(
                                iconData: Icons.star,
                                title: 'Total Favorit',
                                total: state.totalFavorite == null
                                    ? 0
                                    : state.totalFavorite,
                              ),
                              _CountWidget(
                                iconData: Icons.supervised_user_circle_outlined,
                                title: 'Total User',
                                total: state.totalUser == null
                                    ? 0
                                    : state.totalUser,
                              ),
                            ],
                          );
                        },
                      ),
                      SizedBox(height: 30),
                      ElevatedButton(
                        onPressed: () {
                          _profileBloc.signOut();
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('Log Out'),
                            SizedBox(width: 8),
                            Icon(Icons.login_outlined),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
            return SizedBox();
          },
        ),
      ),
    );
  }
}

class _CountWidget extends StatelessWidget {
  final IconData iconData;
  final String title;
  final int total;

  _CountWidget({
    Key key,
    this.iconData,
    this.title,
    this.total,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          iconData,
          size: 48,
          color: AppColor.PRIMARY,
        ),
        Text(
          title,
          style: context.textTheme.bodyText1,
        ),
        Text(
          '${total}',
          style: context.textTheme.headline6,
        ),
      ],
    );
  }
}
