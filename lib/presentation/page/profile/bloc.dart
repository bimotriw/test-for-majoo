import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:meta/meta.dart';

import '../../../domain/repository/people_repository.dart';
import '../../core/app.dart';

// Start of state
abstract class ProfileState extends Equatable {
  final int totalFavorite;
  final int totalUser;

  ProfileState({this.totalUser, this.totalFavorite});

  @override
  List<Object> get props => [totalFavorite, totalUser];
}

class ProfileInitialState extends ProfileState {}

class ProfileLoadingState extends ProfileState {
  ProfileLoadingState({int totalUser, int totalFavorite})
      : super(totalUser: totalUser, totalFavorite: totalFavorite);
}

class ProfileLoadedState extends ProfileState {
  ProfileLoadedState(int totalUser, int totalFavorite)
      : super(totalUser: totalUser, totalFavorite: totalFavorite);
}

class ProfileSignOutState extends ProfileState {}
//End state

class ProfileBloc extends Cubit<ProfileState> {
  final PeopleRepository repository;

  final Logger logger = Logger('ProfileBloc');

  ProfileBloc({@required this.repository}) : super(ProfileInitialState());

  Future<void> getTotal() async {
    emit(ProfileLoadingState());

    List<int> result = await repository.getTotal();

    emit(ProfileLoadedState(result[0], result[1]));
  }

  Future<void> signOut() async {
    emit(ProfileLoadingState(
        totalUser: state.totalUser, totalFavorite: state.totalFavorite));

    await App.main.googleSignIn.disconnect();
    FirebaseAuth.instance.signOut();

    emit(ProfileSignOutState());
  }
}
