import 'package:flutter/material.dart';

import '../../extension/context_extension.dart';
import '../config/style_config.dart';

class LogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Image.asset('assets/images/majoo-logo.png'),
        SizedBox(height: 8),
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 8,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            color: AppColor.BASIC.withOpacity(0.1),
          ),
          child: Text(
            'Job Entry Test',
            style: context.textTheme.headline6,
          ),
        ),
      ],
    );
  }
}
