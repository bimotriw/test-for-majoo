import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../config/style_config.dart';

class CacheAvatarImageWidget extends StatelessWidget {
  final String photoUrl;
  final double radius;

  CacheAvatarImageWidget({
    @required this.photoUrl,
    @required this.radius,
  });

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: photoUrl,
      imageBuilder: (context, imageProvider) => CircleAvatar(
        maxRadius: radius,
        backgroundColor: Colors.transparent,
        backgroundImage: imageProvider,
      ),
      placeholder: (context, url) => Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          border: Border.all(
            width: 1,
            color: Color.fromRGBO(187, 196, 204, 1),
          ),
        ),
        height: radius * 2,
        width: radius * 2,
        child: Center(
            child: CircularProgressIndicator(
          strokeWidth: 2,
        )),
      ),
      errorWidget: (context, url, error) => Container(
        decoration: BoxDecoration(
          color: AppColor.BASIC,
          shape: BoxShape.circle,
        ),
        height: radius * 2,
        width: radius * 2,
        child: Icon(
          Icons.person,
          color: AppColor.BASIC,
          size: radius,
        ),
      ),
    );
  }
}
