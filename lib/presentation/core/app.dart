import 'package:dio/dio.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:google_sign_in/google_sign_in.dart';

class App {
  final String baseUrl;

  Dio client;
  GoogleSignIn googleSignIn;

  App({
    this.baseUrl,
    this.googleSignIn,
  });

  static App get main => GetIt.instance.get<App>();

  Future<void> init() async {
    await Firebase.initializeApp();

    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    client = Dio(BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 50000,
      receiveTimeout: 30000,
      contentType: "application/json; charset=utf-8",
    ));
  }
}
