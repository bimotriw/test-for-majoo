import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/repository_impl/session_repository_impl.dart';
import '../bloc/session_bloc.dart';
import '../config/style_config.dart';
import '../page/splash/page.dart';

class MainApp extends StatefulWidget {
  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SessionBloc(
        repository: SessionRepositoryImpl(),
      ),
      child: MaterialApp(
        theme: defaultStyleConfig,
        debugShowCheckedModeBanner: false,
        home: SplashPage(),
      ),
    );
  }
}
