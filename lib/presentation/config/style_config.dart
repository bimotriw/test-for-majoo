import 'package:flutter/material.dart';

ThemeData get defaultStyleConfig => ThemeData(
      brightness: Brightness.dark,
      unselectedWidgetColor: AppColor.BASIC,
      scaffoldBackgroundColor: Colors.white,
      primaryColor: AppColor.PRIMARY,
      accentColor: AppColor.PRIMARY,
      appBarTheme: AppBarTheme(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        color: AppColor.BASIC,
        centerTitle: true,
        textTheme: TextTheme(
          headline6: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: AppColor.PRIMARY,
      ),
      textTheme: TextTheme(
        headline3: TextStyle(
          color: AppColor.BASIC,
        ),
        headline4: TextStyle(
          color: AppColor.BASIC,
        ),
        headline5: TextStyle(
          color: AppColor.BASIC,
          fontSize: 22,
          fontWeight: FontWeight.bold,
        ),
        headline6: TextStyle(
          color: AppColor.BASIC,
          fontSize: 17,
          fontWeight: FontWeight.w700,
        ),
        subtitle1: TextStyle(
          color: AppColor.BASIC,
          fontSize: 16,
        ),
        subtitle2: TextStyle(
          color: AppColor.BASIC,
        ),
        bodyText1: TextStyle(
          color: AppColor.BASIC,
          fontWeight: FontWeight.w500,
        ),
        bodyText2: TextStyle(
          color: AppColor.BASIC,
        ),
        caption: TextStyle(
          color: AppColor.BASIC,
          fontSize: 12,
        ),
        overline: TextStyle(
          color: AppColor.BASIC,
          fontSize: 12,
          fontWeight: FontWeight.w300,
        ),
      ),
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: TextStyle(
          color: AppColor.PRIMARY,
        ),
        hintStyle: TextStyle(
          color: AppColor.BASIC,
        ),
        contentPadding: EdgeInsets.symmetric(vertical: 17, horizontal: 8),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColor.BASIC, width: 1),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColor.PRIMARY, width: 1),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColor.ERROR, width: 1),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColor.ERROR, width: 1),
        ),
        errorStyle: TextStyle(
          color: AppColor.ERROR,
          fontSize: 12,
        ),
        errorMaxLines: 1,
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.symmetric(
            vertical: 16,
            horizontal: 24,
          ),
          shape: _defaultShape,
          elevation: 0,
          textStyle: TextStyle(
            fontSize: 16,
            height: 16 / 14,
            fontWeight: FontWeight.w700,
          ),
        ).copyWith(
          foregroundColor: MaterialStateProperty.resolveWith(
            (states) => Colors.white,
          ),
          backgroundColor: MaterialStateProperty.resolveWith(
            (states) => _defaultColor(states),
          ),
        ),
      ),
    );

// default shape button
RoundedRectangleBorder get _defaultShape => RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(16),
    );

// default color button for handle disable or enable button
Color _defaultColor(Set<MaterialState> states) {
  if (states.contains(MaterialState.disabled)) {
    return AppColor.BASIC.withOpacity(0.7);
  }
  return AppColor.BASIC;
}

class AppColor {
  static const PRIMARY = Color.fromRGBO(224, 239, 198, 1);
  static const BASIC = Color.fromRGBO(58, 187, 181, 1);
  static const ERROR = Color.fromRGBO(214, 51, 51, 1);
}
