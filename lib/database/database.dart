import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../data/model/people_model.dart';
import '../domain/entity/people_entity.dart';

class NotesDatabase {
  static final NotesDatabase instance = NotesDatabase._init();

  static Database _database;

  NotesDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await _initDB('test-majoo.db');
    return _database;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);
    print(path);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    final textType = 'TEXT NOT NULL';
    final boolType = 'BOOLEAN NOT NULL';

    await db.execute('''
      CREATE TABLE $tablePeople ( 
        ${PeopleFields.id} $idType, 
        ${PeopleFields.name} $textType, 
        ${PeopleFields.height} $textType,
        ${PeopleFields.mass} $textType,
        ${PeopleFields.hairColor} $textType,
        ${PeopleFields.skinColor} $textType,
        ${PeopleFields.eyeColor} $textType,
        ${PeopleFields.birthYear} $textType,
        ${PeopleFields.gender} $textType,
        ${PeopleFields.created} $textType,
        ${PeopleFields.edited} $textType,
        ${PeopleFields.isFavorite} $boolType
        )
      ''');
  }

  Future<PeopleEntity> create(PeopleEntity people) async {
    final db = await instance.database;

    await db.insert(tablePeople, people.toJson());
    return people;
  }

  Future<PeopleEntity> readNote(int id) async {
    final db = await instance.database;

    final maps = await db.query(
      tablePeople,
      columns: PeopleFields.values,
      where: '${PeopleFields.id} = ?',
      whereArgs: [id],
    );

    if (maps.isNotEmpty) {
      return PeopleModel.fromJson(maps.first);
    } else {
      throw Exception('ID $id not found');
    }
  }

  Future<List<PeopleEntity>> readAllNotes(
      {String keyword = '', String order = 'ASC', isFavorite = false}) async {
    final db = await instance.database;

    final orderBy = '${PeopleFields.name} ${order}';

    final isFavoriteCondition =
        isFavorite ? 'AND ${PeopleFields.isFavorite} == 1' : '';

    final result = await db.rawQuery(
        "SELECT * FROM $tablePeople WHERE ${PeopleFields.name} LIKE '%${keyword}%' ${isFavoriteCondition} ORDER BY $orderBy");

    return result.map((json) => PeopleModel.fromJson(json)).toList();
  }

  Future<int> update(PeopleEntity people) async {
    final db = await instance.database;

    return db.update(
      tablePeople,
      people.toJson(),
      where: '${PeopleFields.id} = ?',
      whereArgs: [people.id],
    );
  }

  Future<int> favorite(PeopleEntity people) async {
    final db = await instance.database;

    return db.update(
      tablePeople,
      people.copyWith(isFavorite: people.isFavorite == 0 ? 1 : 0).toJson(),
      where: '${PeopleFields.id} = ?',
      whereArgs: [people.id],
    );
  }

  Future<int> delete(int id) async {
    final db = await instance.database;

    return await db.delete(
      tablePeople,
      where: '${PeopleFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future close() async {
    final db = await instance.database;

    db.close();
  }
}
