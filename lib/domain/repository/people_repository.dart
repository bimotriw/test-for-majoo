import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../core/failure.dart';
import '../entity/film_entity.dart';
import '../entity/people_entity.dart';
import '../entity/species_entity.dart';

abstract class PeopleRepository {
  Future<Either<Failure, List<dynamic>>> getInitialFetch({
    @required int page,
  });

  Future<List<PeopleEntity>> getLocalData({
    @required int page,
  });

  Future<List<PeopleEntity>> saveToLocalData({
    @required List<PeopleEntity> peoples,
  });

  Future<List<PeopleEntity>> changeFavoriteStatus({
    @required PeopleEntity people,
    String keyword,
    String order,
    bool isFavorite,
  });

  Future<List<PeopleEntity>> searchFromLocal({
    @required String keyword,
    String order,
  });

  Future<void> updatePeopleLocal({
    @required PeopleEntity people,
  });

  Future<void> deletePeopleLocal({
    @required int id,
  });

  Future<void> addPeopleLocal({
    @required PeopleEntity people,
  });

  Future<List<int>> getTotal();

  Future<List<PeopleEntity>> getLocalFavorites();

  Future<Either<Failure, List<dynamic>>> getFilmAndSpecies({
    @required int id,
  });

  Future<FilmEntity> getFilmDetail({
    @required int id,
  });

  Future<SpeciesEntity> getSpeciesDetail({
    @required int id,
  });
}
