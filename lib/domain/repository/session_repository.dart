import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../core/failure.dart';
import '../entity/session_entity.dart';

abstract class SessionRepository {
  Future<Either<Failure, SessionEntity>> load();

  Future<Either<Failure, bool>> save({
    @required SessionEntity session,
  });

  Future<Either<Failure, bool>> delete();
}
