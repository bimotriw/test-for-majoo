import 'package:equatable/equatable.dart';

class SpeciesEntity extends Equatable {
  final String name;
  final String classification;
  final String designation;
  final String averageHeight;
  final String skinColors;
  final String hairColors;
  final String eyeColors;
  final String averageLifespan;
  final String homeworld;
  final String language;

  SpeciesEntity({
    this.name,
    this.classification,
    this.designation,
    this.averageHeight,
    this.skinColors,
    this.hairColors,
    this.eyeColors,
    this.averageLifespan,
    this.homeworld,
    this.language,
  });

  @override
  List<Object> get props => [
        name,
        classification,
        designation,
        averageHeight,
        skinColors,
        hairColors,
        eyeColors,
        averageLifespan,
        homeworld,
        language,
      ];
}
