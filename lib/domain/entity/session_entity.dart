import 'package:equatable/equatable.dart';

class SessionEntity extends Equatable {
  final String id;
  final String email;
  final String displayName;
  final String photoUrl;

  SessionEntity({
    this.id,
    this.email,
    this.displayName,
    this.photoUrl,
  });

  @override
  List<Object> get props => [
    id,
    email,
    displayName,
    photoUrl,
  ];
}
