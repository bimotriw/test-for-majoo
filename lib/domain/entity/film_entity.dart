import 'package:equatable/equatable.dart';

class FilmEntity extends Equatable {
  final String title;
  final String openingCrawl;
  final String director;
  final String producer;
  final String releaseDate;

  FilmEntity({
    this.title,
    this.openingCrawl,
    this.director,
    this.producer,
    this.releaseDate,
  });

  @override
  List<Object> get props => [
        title,
        openingCrawl,
        director,
        producer,
        releaseDate,
      ];
}
