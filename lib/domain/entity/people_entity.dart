import 'package:equatable/equatable.dart';

final String tablePeople = 'people';

class PeopleFields {
  static final List<String> values = [
    id,
    name,
    height,
    mass,
    hairColor,
    skinColor,
    eyeColor,
    birthYear,
    gender,
    created,
    edited,
  ];

  static final String id = '_id';
  static final String name = 'name';
  static final String height = 'height';
  static final String mass = 'mass';
  static final String hairColor = 'hairColor';
  static final String skinColor = 'skinColor';
  static final String eyeColor = 'eyeColor';
  static final String birthYear = 'birthYear';
  static final String gender = 'gender';
  static final String created = 'created';
  static final String edited = 'edited';
  static final String isFavorite = 'isFavorite';
}

class PeopleEntity extends Equatable {
  final int id;
  final String name;
  final String height;
  final String mass;
  final String hairColor;
  final String skinColor;
  final String eyeColor;
  final String birthYear;
  final String gender;
  final String created;
  final String edited;
  final int isFavorite;

  PeopleEntity({
    this.id,
    this.name,
    this.height,
    this.mass,
    this.hairColor,
    this.skinColor,
    this.eyeColor,
    this.birthYear,
    this.gender,
    this.created,
    this.edited,
    this.isFavorite,
  });

  @override
  List<Object> get props => [
        id,
        name,
        height,
        mass,
        hairColor,
        skinColor,
        eyeColor,
        birthYear,
        gender,
        created,
        edited,
        isFavorite,
      ];

  PeopleEntity copyWith({
    int id,
    String name,
    String height,
    String mass,
    String hairColor,
    String skinColor,
    String eyeColor,
    String birthYear,
    String gender,
    String created,
    String edited,
    int isFavorite,
  }) =>
      PeopleEntity(
        id: id ?? this.id,
        name: name ?? this.name,
        height: height ?? this.height,
        mass: mass ?? this.mass,
        hairColor: hairColor ?? this.hairColor,
        skinColor: skinColor ?? this.skinColor,
        eyeColor: eyeColor ?? this.eyeColor,
        birthYear: birthYear ?? this.birthYear,
        gender: gender ?? this.gender,
        created: created ?? this.created,
        edited: edited ?? this.edited,
        isFavorite: isFavorite ?? this.isFavorite,
      );

  Map<String, Object> toJson() => {
        PeopleFields.id: id,
        PeopleFields.name: name,
        PeopleFields.height: height,
        PeopleFields.mass: mass,
        PeopleFields.hairColor: hairColor,
        PeopleFields.skinColor: skinColor,
        PeopleFields.eyeColor: eyeColor,
        PeopleFields.birthYear: birthYear,
        PeopleFields.gender: gender,
        PeopleFields.created: created,
        PeopleFields.edited: edited,
        PeopleFields.isFavorite: isFavorite ?? 0,
      };
}
